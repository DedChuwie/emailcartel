#!/usr/bin/env python2.7
#-*- coding: utf-8 -*-

import sys, os, logging, re
import requests
#from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
#	AdaptiveETA, FileTransferSpeed, FormatLabel, Percentage, \
#	ProgressBar, ReverseBar, RotatingMarker, \
#	SimpleProgress, Timer, UnknownLength

class MailRU:
	def __init__(self, user_agent, tasksfile):
		self.request = requests.Session()
		self.request.headers['User-Agent'] = user_agent
		self.timeout = (15, 20)

		self.log_false  = open('logs/false.txt', 'w')
		self.log_true 	= open('logs/true.txt', 'w')
		self.log_error 	= open('logs/error.txt', 'w')
		self.mymail = 'https://my.mail.ru/mail/'

		self.tasks = open(tasksfile,'r').readlines()
		#print self.tasks
		#sys.exit()
		self.taskscount = len(self.tasks)
	def add_log(self):
		pass

	def doit(self, ulogin):
		self.content = self.request.get(url=self.mymail + ulogin + '/', timeout=self.timeout)

		return self.content.status_code

	def run(self):
		#widgets = ['Test: ', Percentage(), ' ', Bar(marker=RotatingMarker()),
		#	   ' ', ETA(), ' ', FileTransferSpeed()]
		#pbar = ProgressBar(widgets=[SimpleProgress()], maxval=17).start()
		#pbar = ProgressBar(widgets=widgets, maxval=self.taskscount).start()
		i = 0
		for task in self.tasks:
			try:
				#pbar.update(i + 1)
				self.request.cookies.clear()
				ulogin, domain = task.strip().split('@')

				status = self.doit( ulogin )

				if status == 404:
					print 'bad;;' + task.strip()
					self.log_false.write(task)
					self.log_false.flush()
					continue

				if status == 200:
					flname = re.findall(r'<title>(.*)</title>', self.content.content,re.MULTILINE)[0].split('-')[0]
					print 'good;;' + task.strip() + ';;' + flname  
					self.log_true.write(task.strip() + ';;' + flname.strip() + "\n" )
					self.log_true.flush()
					continue

			except KeyboardInterrupt:
				sys.exit('THIS IS FIASCO!')

			except Exception, e:
				logging.exception(e)
				logging.error(str(e) + ';;' + task )

				self.log_error.write(task)
				self.log_error.flush()
				continue
		#pbar.finish()
if __name__ == '__main__':
	logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.DEBUG, filename = u'log_mailru_tools.txt')
	user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'

	MailRU( user_agent, sys.argv[1] ).run(  )