#!/usr/bin/env python2.7
#-*- coding: utf-8 -*-

import sys, re, logging

"""
Add hello print with help
Add Redis work
"""

class EmailValidator:
	def __init__(self):
		path = 'dump/validator/'
		self.log_false  = open(path + 'false.txt', 'w')
		self.log_true 	= open(path + 'true.txt', 'w')
		self.log_error 	= open(path + 'error.txt', 'w')

		self.email_list = []

		for email in open(sys.argv[1], 'r'):
			self.email_list.append( email.strip() )

		self.email_list = sorted( set( self.email_list ) )

		self.match = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'

	def run(self):
		for email in self.email_list:
			try:
				match = re.match(self.match, email)

				if match == None:
					self.log_false.write(email + "\n")
					continue

				self.log_true.write(email + "\n")
				continue

			except KeyboardInterrupt:
				sys.exit('THIS IS FIASCO!!')

			except Exception, e:
				logging.exception(e)
				logging.error(str(e) + ';;' + email )

				self.log_error.write(email)
				continue

		self.log_error.flush()
		self.log_true.flush()
		self.log_false.flush()
		sys.exit('Finish: Check files in dump/validator/')

if __name__ == '__main__':
	logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.DEBUG, filename = u'log_validator.txt')

	EmailValidator().run()