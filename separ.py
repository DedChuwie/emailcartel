#!/usr/bin/env python2.7
#-*- coding: utf-8 -*-

import sys, logging
from collections import defaultdict

"""
Add hello print with help
Add Redis work?
"""

class EmailSepar:
	def __init__(self):
		self.path = 'dump/separ/'

		self.email_list = []
		self.domains = defaultdict(list)

		for email in open(sys.argv[1], 'r'):
			self.email_list.append(email.strip())
		self.email_list = sorted(set(self.email_list))

	def run(self):
		try:

			for email in self.email_list:
				domain = email.strip().split('@')[-1]
				self.domains[domain].append(email)

			for domain in self.domains:
				with open(self.path + domain + '.txt', 'w') as fdomain:
					fdomain.write("\n".join(self.domains[domain]).strip())

		except KeyboardInterrupt:
			sys.exit('THIS IS FIASCO!')

		except Exception, e:
			logging.exception(e)

		sys.exit('Finish: Check files in dump/separ/')

if __name__ == '__main__':
	logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.DEBUG, filename = u'log_separ.txt')

	EmailSepar().run()